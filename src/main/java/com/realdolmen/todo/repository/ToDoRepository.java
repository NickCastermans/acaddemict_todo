package com.realdolmen.todo.repository;

import com.realdolmen.todo.domain.ToDo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class ToDoRepository {

    @PersistenceContext
    EntityManager em;

    public List<ToDo> getUncompleteToDos(){
        return em.createQuery("SELECT t FROM ToDo t WHERE t.isComplete = false", ToDo.class).getResultList();
    }

    public List<ToDo> getCompleteToDos(){
        return em.createQuery("SELECT t FROM ToDo t WHERE t.isComplete = true", ToDo.class).getResultList();
    }


    public ToDo findToDoById(long id) {
        return em.find(ToDo.class, id);
    }

    public void saveToDo(ToDo toDo) {
        em.persist(toDo);
    }
    public void updateToDo(ToDo toDo) {
        em.merge(toDo);
    }
    public void deleteToDoById(long id) {
        em.remove(em.getReference(ToDo.class, id));
    }





}
