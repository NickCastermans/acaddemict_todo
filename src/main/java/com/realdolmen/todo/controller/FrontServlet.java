package com.realdolmen.todo.controller;

import org.slf4j.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/*")
public class FrontServlet extends HttpServlet {

    @Inject
    ControllerMapper controllerMapper;

    @Inject
    Logger logger;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = req.getRequestURI();
        url = url.substring(url.indexOf('/', 1)+1);
        AbstractController controller = controllerMapper.getController(url);

        if(controller == null) {
            resp.setStatus(404);
            return;
        }
        controller.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
