package com.realdolmen.todo.services;

import com.realdolmen.todo.domain.ToDo;
import com.realdolmen.todo.repository.ToDoRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class ToDoService {
    @Inject
    ToDoRepository toDoRepository;


    List<ToDo> getUncompleteToDos(){
        return toDoRepository.getUncompleteToDos();
    }

    List<ToDo> getCompleteToDos(){
        return toDoRepository.getCompleteToDos();
    }


    public ToDo findToDoById(long id) {
        return toDoRepository.findToDoById(id);
    }

    public void saveToDo(ToDo toDo) {
        toDoRepository.saveToDo(toDo);
    }
    public void updateToDo(ToDo toDo) {
       toDoRepository.updateToDo(toDo);
    }
    public void deleteToDoById(long id) {
        toDoRepository.deleteToDoById(id);
    }

}
