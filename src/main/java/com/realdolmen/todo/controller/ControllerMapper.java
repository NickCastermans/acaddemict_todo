package com.realdolmen.todo.controller;

import org.reflections.Reflections;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class ControllerMapper {

    @Inject
    Logger logger;

    private Map<String, AbstractController> urlMapping;

    @PostConstruct
    public void init() {
        urlMapping = new HashMap<>();
        for(Class<? extends AbstractController> c: new Reflections("com.realdolmen.todo").getSubTypesOf(AbstractController.class)) {
            UrlPattern urlAnnotation = c.getAnnotation(UrlPattern.class);
            if(urlAnnotation != null) {
                String[] urlPatterns = urlAnnotation.values();
                for(String urlPattern: urlPatterns) {
                    if (urlMapping.containsKey(urlPattern)) {
                        logger.warn("Duplicate entry for pattern {}.", urlPattern);
                    }
                    try {
                        urlMapping.put(urlPattern, c.getConstructor().newInstance());
                        logger.info("Added url pattern {} for class {}.", urlPattern, c);
                    } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                        logger.error("Registering url pattern {} for class {} failed.", urlPattern, c);
                    }
                }
            }
        }
    }

    public AbstractController getController(String url) {
        url = url.trim();
        System.out.println(url+": "+urlMapping.containsKey(url));
        return urlMapping.get(url);
    }

}
